import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

function Ticker({priceChange, bg}) {
  const priceBool = priceChange.toString().slice(0, 1) !== '-';

  const Up = () => <Text style={styles.triangle}>▲</Text>
  const Down = () => <Text style={styles.triangle}>▼</Text>

  const stylePicker = () => {
    if (bg) {
      return priceBool ? styles.greenBg : styles.redBg
    } else {
      return styles.regular
    }
  }
  return (
    <View style={[styles.regular, stylePicker()]}>
      <Text style={{color: priceBool ? 'green' : 'red'}}>
        {priceBool ? <Up/> : <Down/>} {priceChange.toFixed(2)} %
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  triangle: {
    fontSize: 20
  },
  regular: {
    borderRadius: 8,
    paddingLeft: 7,
    maxHeight: 30,
    paddingBottom: 3
  },
  greenBg: {
    backgroundColor: 'rgba(0, 128, 0, 0.3)',
    paddingRight: 7,
  },
  redBg: {
    backgroundColor: 'rgba(255, 0 ,0, 0.3)',
    paddingRight: 7,
  }
});

export default Ticker;
