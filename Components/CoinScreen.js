import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, ToastAndroid, Linking} from 'react-native';
import {Button, ButtonGroup} from 'react-native-elements';
import {LineChart} from 'react-native-svg-charts'
import * as shape from 'd3-shape'

import Ticker from "./Ticker";
import {symbol} from "d3-shape";

const DaysAcronym = {
  1: '24h',
  7: '1W',
  30: '1M',
  365: '1Y',
  9999: 'all',
}

const checkIndexIsEven = (n) =>  n % 2 === 0

export default function CoinScreen({route, navigation}) {
  const {id, name, ath, low_24h, high_24h, market_cap, price_change_percentage_24h, circulating_supply, total_volume} = route.params;
  // set header title
  navigation.setOptions({title: name})


  // local state
  const [coinChart, setCoinChart] = useState([])
  const [days, setDays] = useState(1)
  const [selectedTimeIndex, setSelectedTimeIndex] = useState(0)

  useEffect(() => {
    requestCoinData()
  }, [days])

  const requestCoinData = () => {
    fetch(`https://api.coingecko.com/api/v3/coins/${id}/market_chart?range&vs_currency=eur&days=${days}`)
      .then(res => res.json())
      .then(data => {
        const pricesArray = data.prices.map(p => p[1])
        setCoinChart(pricesArray)
      })
      .catch(err => ToastAndroid.show('Network request failed', ToastAndroid.SHORT))
  }

  const updateTimeRange = (selectedIndex) => {
    setSelectedTimeIndex(selectedIndex)
    const getNumberOfDays = parseInt(Object.keys(DaysAcronym)[selectedIndex])
    setDays(getNumberOfDays)
  }

  const TimePicker = () => (
    <ButtonGroup
      onPress={updateTimeRange}
      selectedIndex={selectedTimeIndex}
      buttons={Object.values(DaysAcronym)}
      containerStyle={styles.btnContainer}
      buttonStyle={styles.singleBtn}
      innerBorderStyle={{color: 'transparent'}}
    />
  )

  const truncatedList = coinChart.filter((el, index) => index % 20 === 19)

  return (
    <View style={styles.container}>
      <View style={styles.topInfo}>
        <Text style={styles.price}>€ {ath.toLocaleString()}</Text>
        <Ticker priceChange={price_change_percentage_24h} bg={true} style={styles.ticker}/>
      </View>
      <View style={styles.lowHigh}>
        <Text style={styles.low}>24h low € <Text style={styles.value}>{low_24h}</Text></Text>
        <Text style={styles.low}>24h high €<Text style={styles.value}>{high_24h}</Text></Text>
      </View>
      <LineChart
        style={{height: 200}}
        data={truncatedList}
        svg={{stroke: 'blue'}}
        contentInset={{top: 20, bottom: 20}}
        curve={shape.curveNatural}
      />
      <TimePicker/>
      <Button
        title={`Buy, Sell or Exchange ${name}`}
        buttonStyle={styles.cta}
        onPress={() => Linking.openURL('https://kriptomat.io')}
      />
      <Text style={styles.overview}>Overview</Text>
      <View style={styles.overviewContainer}>
        <View style={styles.overviewBox}><Text>Volume (1d): </Text>
          <Text style={styles.value}>€ {total_volume}</Text></View>
        <View style={styles.overviewBox}><Text>Market cap: </Text>
          <Text style={styles.value}>€ {market_cap}</Text></View>
        <View style={styles.overviewBox}><Text>Circulating supply: </Text>
          <Text style={styles.value}>{circulating_supply} {symbol}</Text></View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 10
  },
  btnContainer: {
    borderRadius: 4,
    borderColor: 'transparent',
    width: '70%',
    height: 30,
    alignItems: 'center',
    backgroundColor: 'transparent',
    justifyContent: 'space-between'
  },
  singleBtn: {
    textTransform: 'uppercase',
    fontSize: 10,
    borderRadius: 20,
    padding: 0,
    margin: 0
  },
  overviewContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  overviewBox: {
    flexBasis: '50%',
    borderWidth: 1,
    borderColor: '#d6d6d6',
    padding: 5
  },
  overview: {
    fontSize: 18,
    padding: 4,
    fontWeight: 'bold',
    width: '100%'
  },
  value: {
    fontWeight: 'bold',
  },
  cta: {
    marginTop: 10,
    marginBottom: 10,
    padding: 10,
    borderRadius: 8,
  },
  topInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  price: {
    fontSize: 28,
    fontWeight: 'bold',
    color: 'black',
    flex: 1
  },
  ticker: {
    flex: 1
  },
  lowHigh: {
    flexDirection: 'row'
  },
  low: {
    flex: 1
  },
  high: {
    flex: 1
  },
  timePickerWrap: {
    alignItems: 'center'
  }
});
