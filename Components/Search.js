import React, {useRef, useEffect} from 'react';
import {View, TextInput, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

function Search({ setResults, list, setQuery}) {
  const inputEl = useRef(null);

  const onSearch = (query) => {
    const results = list.filter(coin => {
      const fullSearchableName = coin.name.toLowerCase() + ` ${coin.symbol}`
      return fullSearchableName.indexOf(query) > -1;
    })
    setResults(results)
    setQuery(query)
  }

  const clear = () => {
    setQuery('')
    inputEl.current.clear()
  }

  useEffect(() => inputEl.current.focus())

  return (
    <View style={styles.searchSection}>
      <View style={styles.searchIcon}><Icon iconStyle={styles.searchIcon} name="ios-search" size={20} color="gray" /></View>
      <TextInput
        placeholder="Search"
        onChangeText={onSearch}
        style={styles.input}
        ref={inputEl}
      />
      <View style={styles.searchIcon}>
        <TouchableOpacity onPress={() => clear()}>
          <Icon iconStyle={styles.searchIcon} name="close-outline" size={20} color="gray" />
        </TouchableOpacity>
      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  searchSection: {
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'row'
  },
  searchIcon: {
    marginTop: 12,
    flexShrink: 2,
    paddingRight: 3
  },
  input: {
    flexGrow: 7,
    color: 'black',
  }
});


export default Search;
