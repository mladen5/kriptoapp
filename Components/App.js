import React, {useEffect, useState} from 'react';
import {StyleSheet, View, ToastAndroid, Linking} from 'react-native';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Link} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Icon from "react-native-vector-icons/Ionicons";
import {Button, Divider} from 'react-native-elements';
import HomeScreen from './HomeScreen';
import CoinScreen from './CoinScreen';
import SearchScreen from './SearchScreen';

const Stack = createStackNavigator();

export default function App() {
  const [coinsList, setCoinsList] = useState([])
  const [query, setQuery] = useState(null)

  useEffect(() => {
    requestCoins()
  }, [])

  useEffect(() => {
    if (!query) {
      requestCoins()
    }
  }, [query])



  const requestCoins = () => {
    fetch('https://api.coingecko.com/api/v3/coins/markets?vs_currency=eur')
      .then(res => res.json())
      .then(data => setCoinsList(data))
      .catch(err => {
        ToastAndroid.show('Network request failed', ToastAndroid.SHORT)
      })
  }

  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="HomeScreen">
          <Stack.Screen name="HomeScreen" options={{headerShown: false}}>
            {(props) => <HomeScreen {...props} coins={coinsList} setCoinsList={setCoinsList} setQuery={setQuery} />}
          </Stack.Screen>
          <Stack.Screen name="CoinScreen" component={CoinScreen} options={{
            headerRight: () => (
              <View style={styles.searchIcon}>
                <Link to="/SearchScreen">
                  <Icon iconStyle={styles.searchIcon} name="ios-search" size={20} color="gray"/>
                </Link>
              </View>
            ),
          }}/>
          <Stack.Screen name="SearchScreen" options={{title: 'Search'}}>
            {(props) => <SearchScreen {...props} coins={coinsList}/>}
          </Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
      <Divider orientation="horizontal" color="gray"/>
      <Button
        buttonStyle={styles.kriptomat}
        title="Kriptomat account"
        onPress={() => Linking.openURL('https://kriptomat.io')}
      />
    </SafeAreaProvider>
  );
}

const styles = StyleSheet.create({
  kriptomat: {
    borderRadius: 8,
    padding: 2,
  },
  searchIcon: {
    marginRight: 10
  }
});
