import React, {useState, useEffect} from 'react';
import {StyleSheet, View, SafeAreaView} from 'react-native';
import { WithLocalSvg } from 'react-native-svg';
import Search from './Search';
import CoinsList from './CoinsList';

export default function HomeScreen({ coins, setCoinsList, setQuery }) {

  const Logo = () => (
    <View style={styles.logoWrap}>
      <WithLocalSvg
        height="50"
        asset={require('../logo.svg')}
      />
    </View>)


  return (
    <View>
      <SafeAreaView>
        <Logo/>
        <Search setResults={setCoinsList} list={coins} setQuery={setQuery} />
        <CoinsList list={coins} />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  logoWrap: {
    justifyContent: 'center',
    alignItems: 'center',
    top: 5,

  },
  list: {
    marginTop: 20
  }
});
