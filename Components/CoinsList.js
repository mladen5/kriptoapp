import React from 'react';
import { FlatList, Image, Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Divider } from 'react-native-elements';
import Ticker from "./Ticker";

function CoinsList({ list }) {
  const navigation = useNavigation();
  const onPress = (item) => navigation.navigate('CoinScreen', { ...item })

  const renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => onPress(item)} style={styles.item}>
      <View style={styles.identifier}>
        <Image style={styles.coinLogo} source={{ uri: item.image }}  />
        <View style={styles.names}>
          <Text style={styles.name}>{item.name}</Text>
          <Text style={styles.symbol}>{item.symbol.toUpperCase()}</Text>
        </View>
      </View>
      <View style={styles.data}>
        <Text style={styles.price}>€{Number(item.ath).toFixed(2).toLocaleString()}</Text>
        <View style={styles.ticker}><Ticker priceChange={ item.price_change_percentage_24h } /></View>
      </View>
    </TouchableOpacity>
  );

  return(
    <FlatList
      style={styles.list}
      data={list}
      renderItem={renderItem}
      keyExtractor={item => item.id}
      ItemSeparatorComponent={() => <Divider color="#e3e3e3" orientation="horizontal" />}
      ListEmptyComponent={(<Text>No data</Text>)}
    />
  )
}

export default CoinsList;

const styles = StyleSheet.create({
  item: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingTop: 2,
    paddingBottom: 2
  },
  identifier: {
    width: '70%',
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  data: {
    alignSelf: 'flex-end',
  },
  price: {
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'flex-end'
  },
  list: {
    margin: 10
  },
  names: {
    width: '50%',
    paddingLeft: 5
  },
  name: {
    fontWeight: 'bold',
    fontSize: 15
  },
  symbol: {
    color: '#A9A9A9'
  },
  coinLogo: {
    width: 40,
    height: 40,
  },
  ticker: {
    alignSelf: 'flex-end'
  }
});
