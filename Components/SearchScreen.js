import {StatusBar} from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, SafeAreaView, FlatList, Image, TouchableOpacity} from 'react-native';
import Search from './Search';
import CoinsList from "./CoinsList";

export default function SearchScreen({coins}) {
  const [searchableCoins, setSearchableCoins] = useState(coins)

  return (
    <View>
      <SafeAreaView>
        <Search setResults={setSearchableCoins} list={searchableCoins} />
        <CoinsList list={searchableCoins} />
      </SafeAreaView>
    </View>
  );
}

